# Spark: partitionnement de données d'observation astronomiques

Dans ce TP, on considère les données astronomiques vues dans le [précédent TP][tp-spark-2018].

On souhaite partitionner les différentes observations (éléments de `Source`) selon leur coordonnées célestes, d'abord selon leur attribut `decl` (qui correspond à _[declination][declination]_), puis repartionner le résultat selon leur attribut `ra` (qui correspond à _[right ascension][right-ascension]_).
La difficulté est que l'on souhaite ici répartir équitablement les données plutôt que de les répartir dans des intervalles de taille identique.

On va pour cela utiliser utiliser un algorithme inspiré du [Quick Select][quickselect]: il s'agit de couper en deux le RDD selon un élément choisi comme pivot.

## Remarques préliminaires

Dans ce TP, on va pousser plus loin l'utilisation de Spark et travailler sur une volumétrie de données plus importante.

> Ce TP manipulant éventuellement de grosses données il faut:
>
> - Supprimer les répertoires HDFS inutiles: `hdfs dfs -rm -r -skipTrash repertoire-a-supprimer`
> - Lancer ensuite `hdfs dfs -expunge` pour vider régulièrement la poubelle HDFS (ignorer les erreurs de permissions), en particulier si vous avez oublié `-skipTrash` à l'étape précédente

L'utilitaire [`screen`](https://en.wikipedia.org/wiki/GNU_Screen) permet (entre autres) de conserver un shell actif tout en se déconnectant ([quickref](http://aperiodic.net/screen/quick_reference)).
Il ne faut pas hésiter à l'utiliser quand on lance des jobs longs sur le cluster.

Pour démarrer ce TP, il est possible de reprendre votre code du TP précédent ou celui du corrigé: https://forge.univ-lyon1.fr/EMMANUEL.COQUERY/tp-spark-2018-correction

## Découpage en deux parties égales

Dans un premier temps, on va simplement couper en deux parties (appelées X et Y) de taille égales le RDD initial.
On supposera que X contient les éléments ayant la valeur la plus faible pour l'attribut considéré, Y contenant ceux ayant la valeur la plus élevée.

### Principe

Dans un premier RDD (appelé A par la suite), on placera les éléments plus petits ou égaux au pivot et dans un deuxième RDD (appelé B par la suite), ceux qui sont strictement plus grands.
Évidement, le pivot a toutes les chances d'être différent de l'élément médian.
Cela veut dire qu'un des deux RDD (contenant moins de la moitié des éléments) aura tous ses éléments dans la même partie alors que l'autre aura des éléments des deux parties.

> Supposons que l'ensemble initial contienne 40 000 éléments et qu'après découpage, A contienne 15 000 éléments et B en contienne 25 000.
> Tous les éléments de A seront dans X alors que ceux de B seront à répartir entre X et Y.

On réapplique ensuite récursivement la procédure de découpage sur le RDD qui reste à distribuer.
A ce moment, il faut garder en tête qu'il y a déjà une des deux parties à laquelle on a déjà moralement affecté des éléments, donc l'appel récursif ne devra pas répartir équitablement les éléments.
Par contre on peut savoir combien d'éléments seront à placer dans X et Y.

> En continuant l'exemple précédent, on appelle récursivement la fonction de découpage sur B.
> Il faudra placer 5 000 éléments de B dans X et 20 000 dans Y.
> Soit A' et B' les RDD obtenus après le découpage de B.
> Si A' contient plus de 5 000 éléments, il faudra redécouper A' en deux, alors que s'il contient moins de 5 000 éléments, c'est B' qu'il faudra recouper en deux.

On arrête la récursion quand le RRD est suffisement petit pour être traité en mémoire (cette limite est à définir par un paramètre).
Dans le cas d'un tel petit RDD, on se contentera de le transformer en tableau que l'on triera en mémoire. On prendra ensuite les _k_ premiers éléments pour les placer dans X et on placera le restant dans Y.

L'appel récursif renverra une paire de RDDs (un pour X et un pour Y).
Il faudra faire l'union d'un de ces RDDs avec A ou avec B selon le cas.

On peut remarquer qu'il existe des cas limite qui peuvent être problématiques si on a trop de valeurs égales à celle du pivot. 
Pour traiter ce cas, on peut placer toutes les lignes dont la valeur est égale au pivot dans un tableau séparé et répartir les éléments de ce tableau entre X et Y en fonction des quantités voulues pour ces derniers.

### Implémentation

Implémenter une fonction qui lit un fichier ou un répertoire de sources, le découpera en deux RDDs de taille égales et renverra la paire de RDDs.
Le prototype pourra par exemple ressembler à:

```scala
partionne_en_deux
  (rdd: RDD[(Double, Array[String])], nb_X: Long, nb_Y: Long, sc: SparkContext):
  (RDD[(Double, Array[String])], RDD[(Double, Array[String])])`
```

Ici on utilise des RDD contenant une paire constituée d'un Double (la valeur de l'attribut utilisé pour répartir les lignes) et d'un tableau de String (la ligne).

On commencera par mettre au point le code en local en utilisant des test unitaires, comme au premier TP.
On écrira ensuite une application spark qui écrira chacuns des deux RDDs résultants du découpage selon l'attribut `decl` dans un répertoire qui lui est dédié, par exemple `/user/p1234567/Source/partionned/1` et `/user/p1234567/Source/partionned/2`.
On testera l'application sur le cluster sur le fichier `/tp-data/Source/Source-001`.
Si le résultat est concluant, on testera sur le répertoire `/tp-data/Source`en entier.

Quelques méthodes utiles:

- dans la classe `RDD`:
  - pour choisir le pivot, il est possible de prendre un élément aléatoire dans un RDD, via `takeSample`
  - `filter(f)` permet de ne garder que les éléments d'un RDD pour lesquels `f` renverra  `true`
  - `cache()` permet de réutiliser un RDD, par exemple pour récupérer 2 RDD résultant du filtrage du RDD de départ avec des conditions différentes
  - `count()` pour calculer la taille d'un RDD
  - `++` entre deux RDDs pour obtenir un RDD qui est l'union disjointe des deux
  - `collect()` permet de transformer un RDD en tableau. A n'utiliser que pour des **petits** RDDs (quelques centaines d'éléments).
- dans la classe `SparkContext`:
  - `parallelize(coll)` prend une collection comme un tableau ou un séquence et en fait un RDD.

## Généralisation

On veut maintenant généraliser l'approche pour découper un RDD (de taille _m_) en _N_ parties de taille égales.
On numérotera ces parties par rapport à l'ordre des éléents qu'elles contiennent: la n°1 contient les _m/N_ premiers éléments; la n°2 contient les _m/N_ suivants, etc.
On utilisera le principe précédent (découpage en deux vis à vis d'un pivot), mais avec les observations suivantes:

- Il est possible (et même probable au début) qu'il faille redécouper les deux RDD A et B.
- Un certain RDD intermédiaire verra ses éléments distribués sur un certain nombre de parties numérotées consécutivement, il faut donc savoir au moins quelle partie est la première à recevoir des éléments et laquelle est la dernière.
  Il faut également savoir pour ces deux parties le nombre d'éléments sont encore à placer dedans.
- Cela peut être recalculé, mais il peut être utile de connaître le nombre d'éléments à placer dans chaque partie.

## Requêtage

### Index

Effectuer le partionnement de `Sources` en 10 parties selon la valeur de `decl`.
Redécouper chacune de ces parties en 10 sous-partie selon la valeur de `ra`.
Créer un fichier d'index qui associera à chacune des 100 sous-parties les valeurs minimales et maximales de `decl` et `ra`.

### Requête de zone

Créer une fonction qui produit un RDD contenant les observations à l'intérieur d'un rectangle de coordonnés en procédant comme suit:

- Lire le fichier d'index pour récupérer les sous-parties qui intersectent le rectangle de coordonnées.
- Lire chaque sous-partie concernée et filtrer pour ne garder que les observations dans le rectangle.
- Retourner l'ensemble des observations sélectionnées dans un seul RDD.

## Rendu

Il est attendu un rendu par binôme sous forme d'un fichier zip contenant:

- Le code source
- Un fichier `COMPTE-RENDU.md` contenant les noms, prénoms et numéros des étudiants du binôme.
  Il contiendra également une explication de l'usage de l'application de partionnement, ainsi que des choix effectués lors du développement de celle-ci.
  Il pourra également contenir des remarques additionnelles libres.

Ce zip est à déposer sur tomuss dans la case `rendu_tp_spark2` au plus tard le dimanche 15/11/2020 à 23h59.

[tp-spark-2018]: https://forge.univ-lyon1.fr/EMMANUEL.COQUERY/tp-spark-2018-correction
[declination]: https://fr.wikipedia.org/wiki/Syst%C3%A8me_de_coordonn%C3%A9es_%C3%A9quatoriales
[right-ascension]: https://fr.wikipedia.org/wiki/Syst%C3%A8me_de_coordonn%C3%A9es_%C3%A9quatoriales
[quickselect]: https://fr.wikipedia.org/wiki/Quickselect
